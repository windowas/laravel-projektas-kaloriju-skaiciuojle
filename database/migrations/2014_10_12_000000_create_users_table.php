<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->integer('age')->nullable();
            $table->boolean('gen')->nullable();
            $table->integer('cen')->nullable();
            $table->integer('weight')->nullable();
            $table->integer('loa')->nullable();
            $table->integer('cneed')->nullable();
            $table->integer('consumed')->default(0);
            $table->integer('fneed')->nullable();
            $table->integer('fconsumed')->default(0);
            $table->integer('pneed')->nullable();
            $table->integer('pconsumed')->default(0);
            $table->integer('crneed')->nullable();
            $table->integer('crconsumed')->default(0);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
