<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_id = auth()->user()->id;
        $user = User::find($user_id);


        return view('home')->with('user', $user);
    }

    public function show($id)
    {
        $user_id = auth()->user()->id;
        $user = User::find($user_id);

        return view('user.show')->with('user', $user);
    }

    public function create()
    {
        $user_id = auth()->user()->id;
        $user = User::find($user_id);
        return view('user.create')->with('user', $user);
    }

    public function store(Request $request)
    {

    }

    public function edit($id)
    {
        $user = User::find($id);
        // check for current user
        if(auth()->user()->id !== $user->id){
          return redirect('/')->with('error', 'Unauthorized Action');
        }
        return view('user.edit')->with('user', $user);
    }


    public function update(Request $request, $id)
    {
        $user = User::find($id);
        if(auth()->user()->id !== $user->id){
          return redirect('/')->with('error', 'Unauthorized Action');
        }
        // kaloriju skaiciavimas
        if ($request->input('gen') == 0) {
            $fd = (10 * $request->input('weight')) + (6.25 * $request->input('cen')) - (5 * $request->input('age')) + 5;
          }
        else {
            $fd = (10 * $request->input('weight')) + (6.25 * $request->input('cen')) - (5 * $request->input('age')) - 161;
          }

        switch ($request->input('loa')) {
            case "0":
                $cneed = floor($fd * 1.2);
                break;
            case "1":
                $cneed = floor($fd * 1.375);
                break;
            case "2":
                $cneed = floor($fd * 1.53);
                break;
            case "3":
                $cneed = floor($fd * 1.725);
                break;
            case "4":
                $cneed = floor($fd * 1.9);
                break;
        }
        $fneed = floor(($cneed * 0.25) / 9);
        $pneed = floor(($cneed * 0.25) / 4);
        $crneed =floor(($cneed * 0.25) / 4);

        if ($request->input('age') || $request->input('gen') || $request->input('cen') || $request->input('weight') || $request->input('lao')){
        $user->age = $request->input('age');
        $user->gen = $request->input('gen');
        $user->cen = $request->input('cen');
        $user->weight = $request->input('weight');
        $user->loa = $request->input('loa');
        $user->cneed = $cneed;
        $user->fneed = $fneed;
        $user->pneed = $pneed;
        $user->crneed = $crneed;
        }

        $user->save();
        return view('user.show')->with('user', $user);
    }
}
