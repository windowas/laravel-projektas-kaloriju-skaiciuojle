<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::resource('user', 'HomeController');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/user/{id}',[
    'as' => 'show',
    'uses' => 'HomeController@show'
]);

Route::get('/get-doctors', function () {
  // uzduotis grazinti viuss daktarus
    $daktarai = User::all();
    return response::json($daktarai);
});

// Post add consumed value to db
Route::post('/ajaxpost', function(){
    if(Request::ajax()){
        $user_id = auth()->user()->id;
        $user = App\User::find($user_id);
        $user->consumed += Request::input('consumed');
        $user->pconsumed += Request::input('pconsumed');
        $user->fconsumed += Request::input('fconsumed');
        $user->crconsumed += Request::input('crconsumed');
        if (Request::input('reset') == 1) {
            $user->consumed = 0;
            $user->pconsumed = 0;
            $user->fconsumed = 0;
            $user->crconsumed = 0;
        }
        $user->save();
        return response()->json( array(
        'userpneed' => $user->pneed,
        'userpcon' => $user->pconsumed,
        'userfneed' => $user->fneed,
        'userfcon' => $user->fconsumed,
        'usercrneed' => $user->crneed,
        'usercrcon' => $user->crconsumed,
        'usercon'=> $user->consumed,
        'userneed' => $user->cneed), 200);
    }
});
