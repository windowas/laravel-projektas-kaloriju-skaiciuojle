var cneed;
var fneed;
var crneed;
var pneed;
var aneed;
var fd;

$('#calcBtn').on("click", function cc() {
    var age = document.getElementById("age").value;
    var cm = document.getElementById("cen").value;
    var weight = document.getElementById("weight").value;
    if (age != '' && cm != '' && weight != '') {
        var loa = document.getElementById("loa").value;
        if (document.getElementById("male").checked) {
            fd = (10 * weight) + (6.25 * cm) - (5 * age) + 5;
        } else {
            fd = (10 * weight) + (6.25 * cm) - (5 * age) - 161;
        }
        switch (loa) {
            case "1":
                cneed = fd * 1.2;
                break;
            case "2":
                cneed = fd * 1.375;
                break;
            case "3":
                cneed = fd * 1.53;
                break;
            case "4":
                cneed = fd * 1.725;
                break;
            case "5":
                cneed = fd * 1.9;
                break;
        }
        cneed = Math.floor(cneed);
        fneed = Math.floor((cneed * 0.25) / 9);
        pneed = Math.floor((cneed * 0.25) / 4);
        crneed = Math.floor((cneed * 0.25) / 4);
        // aneed = Math.floor((cneed * 0.25) / 7);
        document.getElementById("rc").value = " " + cneed;
        document.getElementById("rf").value = " " + fneed;
        document.getElementById("rp").value = " " + pneed;
        document.getElementById("rh").value = " " + crneed;
        // document.getElementById("ra").value = " " + aneed;
        document.getElementById("l1").innerHTML = "gramų";
        document.getElementById("l2").innerHTML = "gramų";
        document.getElementById("l3").innerHTML = "gramų";
        // document.getElementById("l4").innerHTML = "gramų";
        var caltype = document.getElementById("caltype").value;
        if (caltype == 'g') {
            document.getElementById("l1").innerHTML = "gramų";
            document.getElementById("l2").innerHTML = "gramų";
            document.getElementById("l3").innerHTML = "gramų";
            // document.getElementById("l4").innerHTML = "gramų";
        }
        if (caltype == 'kg') {
            fat2 = fneed / 1000;
            pro2 = pneed / 1000;
            car2 = crneed / 1000;
            // alh2 = aneed / 1000;
            fat2 = fat2.toFixed(3);
            pro2 = pro2.toFixed(3);
            car2 = car2.toFixed(3);
            // alh2 = alh2.toFixed(3);
            document.getElementById("rf").value = " " + fat2;
            document.getElementById("rp").value = " " + pro2;
            document.getElementById("rh").value = " " + car2;
            // document.getElementById("ra").value = " " + alh2;
            document.getElementById("l1").innerHTML = "kilogramo";
            document.getElementById("l2").innerHTML = "kilogramo";
            document.getElementById("l3").innerHTML = "kilogramo";
            // document.getElementById("l4").innerHTML = "kilogramo";
        }
    } else {
        alert("Prašome užpildyti duomenis tinkamai!");
    }
});

function isNumberKey(id) {
    var no = eval('"' + id + '"');
    var number = document.getElementById(no).value;
    if (!number.match(/^[0-9\.]+$/) && number != "") {
        number = number.substring(0, number.length - 1);
        document.getElementById(id).value = number;
    }
}

function convert() {
    var age = parseInt(document.getElementById("age").value);
    var cm = document.getElementById("cen").value;
    var weight = document.getElementById("weight").value;
    if (age != '' && cm != '' && weight != '') {
        var caltype = document.getElementById("caltype").value;
        var fat = document.getElementById("rf").value;
        var pro = document.getElementById("rp").value;
        var car = document.getElementById("rh").value;
        // var alh = document.getElementById("ra").value;
        if (caltype == 'g') {
            document.getElementById("rc").value = " " + cneed;
            document.getElementById("rf").value = " " + fneed;
            document.getElementById("rp").value = " " + pneed;
            document.getElementById("rh").value = " " + crneed;
            // document.getElementById("ra").value = " " + aneed;
            document.getElementById("l1").innerHTML = "gramų";
            document.getElementById("l2").innerHTML = "gramų";
            document.getElementById("l3").innerHTML = "gramų";
            // document.getElementById("l4").innerHTML = "gramų";
        }
        if (caltype == 'kg') {
            fat2 = fneed / 1000;
            pro2 = pneed / 1000;
            car2 = crneed / 1000;
            // alh2 = aneed / 1000;
            fat2 = fat2.toFixed(3);
            pro2 = pro2.toFixed(3);
            car2 = car2.toFixed(3);
            // alh2 = alh2.toFixed(3);
            document.getElementById("rf").value = " " + fat2;
            document.getElementById("rp").value = " " + pro2;
            document.getElementById("rh").value = " " + car2;
            // document.getElementById("ra").value = " " + alh2;
            document.getElementById("l1").innerHTML = "kilogramo";
            document.getElementById("l2").innerHTML = "kilogramo";
            document.getElementById("l3").innerHTML = "kilogramo";
            // document.getElementById("l4").innerHTML = "kilogramo";
        }
    } else {
        alert("Prašome užpildyti duomenis tinkamai!");
    }
}


// ============== Modals ============
// Get the modal
var modal = document.getElementById('heartModal');
var calcmodal = document.getElementById('thecalculator');
var btn = $("#firstcircle svg, #firstcircle div.background, #firstcircle label");
var calbtn = $("#callCalc");

btn.on("click", function(){
    modal.style.display = "block";
});


calbtn.on("click", function(){
    calcmodal.style.display = "block";
});
// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == calcmodal) {
        calcmodal.style.display = "none";
    }
    else if (event.target == modal){
    modal.style.display = "none";
    }
};

// ------------------consumpsion info ------------------

 var button = $('.clickable');
 var infoDisplay = $('#buttonInfo');
 button.hover(function () {
   var inputs = $(this).children('input');
   var label = $(this).children('label').text();
   arr = [];
   for(var i=0, len=inputs.length; i<len; i++){
     if(inputs[i].type === "hidden"){
       arr.push(inputs[i].value);
     }
   }

   infoDisplay.html("100g "+label+" turi: <br/>"+"Kilokalorijų: "+arr[3]+" kcal"+"<br/>"+" Baltymų: "+arr[0]+" g"+"<br/>"+" Riebalų: "+arr[1]+" g"+"<br/>"+" Angliavandenių: "+arr[2]+" g");
 });
