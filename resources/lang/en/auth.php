<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Tokio vartotojo nėra.',
    'throttle' => 'Per daug kartų bandėte prisijungti. Pabandykite už :seconds s.',

];
