<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Slaptažodis turi būti bent iš 6 ženklų ir turi sutapti su slaptažodžio patvirtinimu.',
    'reset' => 'Jūsų slaptažodis atnaujintas!',
    'sent' => 'Slaptažodžio atstatymo nuorodą rasite pašte!',
    'token' => 'Slaptažodžio atstatymas nebegalioja.',
    'user' => "Tokio elektroninio pašto nėra.",

];
