<div class="row wBg">
  <div class="col-md-4 col-md-offset-4">
    <div id="firstcircle" class="circle">
      <div class="background">
        Kilokalorijos
      </div>
        @if(!$user->cneed == null)
            <label for="firstcircle"><span class="counterMain" data-count="{{$user->consumed}}">{{$user->consumed}}</span>/{{$user->cneed}}</label>
        @endif
      <svg xmlns="http://www.w3.org/2000/svg" version="1.1" x="0px" y="0px" viewBox="0 0 100 100">
        <path fill-opacity="0" stroke-width="1" stroke="#bbb" d="M81.495,13.923c-11.368-5.261-26.234-0.311-31.489,11.032C44.74,13.612,29.879,8.657,18.511,13.923  C6.402,19.539,0.613,33.883,10.175,50.804c6.792,12.04,18.826,21.111,39.831,37.379c20.993-16.268,33.033-25.344,39.819-37.379  C99.387,33.883,93.598,19.539,81.495,13.923z"/>
        <path id="heart-path" fill-opacity="0" stroke-width="3" stroke="#ED6A5A" d="M81.495,13.923c-11.368-5.261-26.234-0.311-31.489,11.032C44.74,13.612,29.879,8.657,18.511,13.923  C6.402,19.539,0.613,33.883,10.175,50.804c6.792,12.04,18.826,21.111,39.831,37.379c20.993-16.268,33.033-25.344,39.819-37.379  C99.387,33.883,93.598,19.539,81.495,13.923z"/>
      </svg>
      <button id="reset" class="reset btn btnCancel">
        Iš naujo nustayti progresą
      </button>
    </div>
  </div>
</div>
<div class="row wBg">
  <div class="col-md-2">
    <div id="ProteinCircle" class="circle circleSmall">
      <div class="background">
        Baltymai
      </div>
        @if(!$user->pneed == null)
          <label for="ProteinCircle"><span class="counterProtein"     data-count="{{$user->pconsumed}}">{{$user->pconsumed}}</span>/{{$user->pneed}}</label>
        @endif
      <svg xmlns="http://www.w3.org/2000/svg" version="1.1" x="0px" y="0px" viewBox="0 0 100 100">
        <path fill-opacity="0" stroke-width="1" stroke="#bbb" d="M81.495,13.923c-11.368-5.261-26.234-0.311-31.489,11.032C44.74,13.612,29.879,8.657,18.511,13.923  C6.402,19.539,0.613,33.883,10.175,50.804c6.792,12.04,18.826,21.111,39.831,37.379c20.993-16.268,33.033-25.344,39.819-37.379  C99.387,33.883,93.598,19.539,81.495,13.923z"/>
        <path id="heart-paths-protein" fill-opacity="0" stroke-width="3" stroke="#ED6A5A" d="M81.495,13.923c-11.368-5.261-26.234-0.311-31.489,11.032C44.74,13.612,29.879,8.657,18.511,13.923  C6.402,19.539,0.613,33.883,10.175,50.804c6.792,12.04,18.826,21.111,39.831,37.379c20.993-16.268,33.033-25.344,39.819-37.379  C99.387,33.883,93.598,19.539,81.495,13.923z"/>
      </svg>
    </div>
  </div>
  <div class="col-md-2 col-md-offset-3">
    <div id="FatCircle" class="circle circleSmall">
      <div class="background">
        Riebalai
      </div>
      @if(!$user->fneed == null)
          <label for="FatCircle"><span class="counterFat" data-count="{{$user->fconsumed}}">{{$user->fconsumed}}</span>/{{$user->fneed}}</label>
      @endif
      <svg xmlns="http://www.w3.org/2000/svg" version="1.1" x="0px" y="0px" viewBox="0 0 100 100">
        <path fill-opacity="0" stroke-width="1" stroke="#bbb" d="M81.495,13.923c-11.368-5.261-26.234-0.311-31.489,11.032C44.74,13.612,29.879,8.657,18.511,13.923  C6.402,19.539,0.613,33.883,10.175,50.804c6.792,12.04,18.826,21.111,39.831,37.379c20.993-16.268,33.033-25.344,39.819-37.379  C99.387,33.883,93.598,19.539,81.495,13.923z"/>
        <path id="heart-paths-fat" fill-opacity="0" stroke-width="3" stroke="#ED6A5A" d="M81.495,13.923c-11.368-5.261-26.234-0.311-31.489,11.032C44.74,13.612,29.879,8.657,18.511,13.923  C6.402,19.539,0.613,33.883,10.175,50.804c6.792,12.04,18.826,21.111,39.831,37.379c20.993-16.268,33.033-25.344,39.819-37.379  C99.387,33.883,93.598,19.539,81.495,13.923z"/>
      </svg>
    </div>
  </div>
  <div class="col-md-2 col-md-offset-3">
    <div id="CarbonCircle" class="circle circleSmall">
      <div class="background">
        Angliavandeniai
      </div>
      @if(!$user->crneed == null)
          <label for="CarbonCircle"><span class="counterCarbon" data-count="{{$user->crconsumed}}">{{$user->crconsumed}}</span>/{{$user->crneed}}</label>
      @endif
      <svg xmlns="http://www.w3.org/2000/svg" version="1.1" x="0px" y="0px" viewBox="0 0 100 100">
        <path fill-opacity="0" stroke-width="1" stroke="#bbb" d="M81.495,13.923c-11.368-5.261-26.234-0.311-31.489,11.032C44.74,13.612,29.879,8.657,18.511,13.923  C6.402,19.539,0.613,33.883,10.175,50.804c6.792,12.04,18.826,21.111,39.831,37.379c20.993-16.268,33.033-25.344,39.819-37.379  C99.387,33.883,93.598,19.539,81.495,13.923z"/>
        <path id="heart-paths-carbon" fill-opacity="0" stroke-width="3" stroke="#ED6A5A" d="M81.495,13.923c-11.368-5.261-26.234-0.311-31.489,11.032C44.74,13.612,29.879,8.657,18.511,13.923  C6.402,19.539,0.613,33.883,10.175,50.804c6.792,12.04,18.826,21.111,39.831,37.379c20.993-16.268,33.033-25.344,39.819-37.379  C99.387,33.883,93.598,19.539,81.495,13.923z"/>
      </svg>
    </div>
  </div>
</div>
