<nav class="navbar navbar-top ">
    <div class="navbar-header">
    <!-- Branding Image -->
        <a class="navbar-brand" href="{{ url('/') }}">
            {{ config('app.name', 'Skaičiuoklė') }}
        </a>
    </div>
        <!-- Right Side Of Navbar -->
    <ul class="nav navbar-nav navbar-right">
        <!-- Authentication Links -->
        @guest
            <li><a class="skctext" href="{{ route('login') }}">Prisijungti</a></li>
            <li><a class="skctext" href="{{ route('register') }}">Registruotis</a></li>
        @else
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                    {{ Auth::user()->name }} <span class="caret"></span>
                </a>
                <ul class="dropdown-menu" role="menu">
                    <li>
                        <a href="{{route('show', auth()->user()->id)}}">
                            Profilis
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">
                                Atsijungti
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                        </form>
                    </li>
                </ul>
            </li>
        @endguest
        </ul>
</nav>
