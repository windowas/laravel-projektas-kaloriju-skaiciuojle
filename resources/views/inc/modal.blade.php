<div id="heartModal" class="modal">
  <!-- Modal content -->
  <div class="modal-content">
    @php
    require_once $_SERVER['DOCUMENT_ROOT'].'/Classes/PHPExcel/IOFactory.php';

    $excelFile = "produktai3.xlsx";

    $objReader = PHPExcel_IOFactory::createReader('Excel2007');
    $objPHPExcel = $objReader->load($excelFile);

    //Itrating through all the sheets in the excel workbook and storing the array data
    foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
        $arrayData[$worksheet->getTitle()] = $worksheet->toArray();
    }
    @endphp

    <div class="row">
      <div class="col-md-9">
        @foreach ($arrayData as $key)
          @foreach ($key as $k => $v)
            <div class="form-group btn clickable">
              {{Form::label('consumed', $v[0])}}
              {{Form::hidden('consumed', $v[1], false, [$user->pconsumed])}}
              {{Form::hidden('consumed', $v[2], false, [$user->fconsumed])}}
              {{Form::hidden('consumed', $v[3], false, [$user->crconsumed])}}
              {{Form::hidden('consumed', $v[4], false, [$user->consumed])}}
            </div>
            @endforeach
          @endforeach
      </div>
      <div class="col-md-3">
        <div class="row">
          <div id="secondcircle" class="circle">
            <div class="background">
              Kilokalorijos
            </div>
            @if(!$user->cneed == null)
                <label id="labelForSecondCircle" for="secondcircle"><span class="counterMain" data-count="{{$user->consumed}}">{{$user->consumed}}</span>/{{$user->cneed}}</label>
            @endif
              <svg xmlns="http://www.w3.org/2000/svg" version="1.1" x="0px" y="0px" viewBox="0 0 100 100">
                <path fill-opacity="0" stroke-width="1" stroke="#bbb" d="M81.495,13.923c-11.368-5.261-26.234-0.311-31.489,11.032C44.74,13.612,29.879,8.657,18.511,13.923  C6.402,19.539,0.613,33.883,10.175,50.804c6.792,12.04,18.826,21.111,39.831,37.379c20.993-16.268,33.033-25.344,39.819-37.379  C99.387,33.883,93.598,19.539,81.495,13.923z"/>
                <path id="heart-paths" fill-opacity="0" stroke-width="3" stroke="#ED6A5A" d="M81.495,13.923c-11.368-5.261-26.234-0.311-31.489,11.032C44.74,13.612,29.879,8.657,18.511,13.923  C6.402,19.539,0.613,33.883,10.175,50.804c6.792,12.04,18.826,21.111,39.831,37.379c20.993-16.268,33.033-25.344,39.819-37.379  C99.387,33.883,93.598,19.539,81.495,13.923z"/>
              </svg>
          </div>
        </div>
        <div id="secondSmallCirlces" class="row">
          <div class="col-md-4">
            <div id="secondProteinCircle" class="circle circleSmall">
              <div class="background">
                Baltymai
              </div>
                @if(!$user->pneed == null)
                  <label for="secondProteinCircle"><span class="counterProtein" data-count="{{$user->pconsumed}}">{{$user->pconsumed}}</span>/{{$user->pneed}}</label>
                @endif
              <svg xmlns="http://www.w3.org/2000/svg" version="1.1" x="0px" y="0px" viewBox="0 0 100 100">
                <path fill-opacity="0" stroke-width="1" stroke="#bbb" d="M81.495,13.923c-11.368-5.261-26.234-0.311-31.489,11.032C44.74,13.612,29.879,8.657,18.511,13.923  C6.402,19.539,0.613,33.883,10.175,50.804c6.792,12.04,18.826,21.111,39.831,37.379c20.993-16.268,33.033-25.344,39.819-37.379  C99.387,33.883,93.598,19.539,81.495,13.923z"/>
                <path id="heart-paths-second-protein" fill-opacity="0" stroke-width="3" stroke="#ED6A5A" d="M81.495,13.923c-11.368-5.261-26.234-0.311-31.489,11.032C44.74,13.612,29.879,8.657,18.511,13.923  C6.402,19.539,0.613,33.883,10.175,50.804c6.792,12.04,18.826,21.111,39.831,37.379c20.993-16.268,33.033-25.344,39.819-37.379  C99.387,33.883,93.598,19.539,81.495,13.923z"/>
              </svg>
            </div>
          </div>
          <div class="col-md-4">
            <div id="secondFatCircle" class="circle circleSmall">
              <div class="background">
                Riebalai
              </div>
              @if(!$user->fneed == null)
                  <label for="secondFatCircle"><span class="counterFat" data-count="{{$user->fconsumed}}">{{$user->fconsumed}}</span>/{{$user->fneed}}</label>
              @endif
              <svg xmlns="http://www.w3.org/2000/svg" version="1.1" x="0px" y="0px" viewBox="0 0 100 100">
                <path fill-opacity="0" stroke-width="1" stroke="#bbb" d="M81.495,13.923c-11.368-5.261-26.234-0.311-31.489,11.032C44.74,13.612,29.879,8.657,18.511,13.923  C6.402,19.539,0.613,33.883,10.175,50.804c6.792,12.04,18.826,21.111,39.831,37.379c20.993-16.268,33.033-25.344,39.819-37.379  C99.387,33.883,93.598,19.539,81.495,13.923z"/>
                <path id="heart-paths-second-fat" fill-opacity="0" stroke-width="3" stroke="#ED6A5A" d="M81.495,13.923c-11.368-5.261-26.234-0.311-31.489,11.032C44.74,13.612,29.879,8.657,18.511,13.923  C6.402,19.539,0.613,33.883,10.175,50.804c6.792,12.04,18.826,21.111,39.831,37.379c20.993-16.268,33.033-25.344,39.819-37.379  C99.387,33.883,93.598,19.539,81.495,13.923z"/>
              </svg>
            </div>
          </div>
          <div class="col-md-4">
            <div id="secondCarbonCircle" class="circle circleSmall">
              <div class="background">
                Angliavandeniai
              </div>
              @if(!$user->crneed == null)
                  <label for="secondCarbonCircle"><span class="counterCarbon" data-count="{{$user->crconsumed}}">{{$user->crconsumed}}</span>/{{$user->crneed}}</label>
              @endif
              <svg xmlns="http://www.w3.org/2000/svg" version="1.1" x="0px" y="0px" viewBox="0 0 100 100">
                <path fill-opacity="0" stroke-width="1" stroke="#bbb" d="M81.495,13.923c-11.368-5.261-26.234-0.311-31.489,11.032C44.74,13.612,29.879,8.657,18.511,13.923  C6.402,19.539,0.613,33.883,10.175,50.804c6.792,12.04,18.826,21.111,39.831,37.379c20.993-16.268,33.033-25.344,39.819-37.379  C99.387,33.883,93.598,19.539,81.495,13.923z"/>
                <path id="heart-paths-second-carbon" fill-opacity="0" stroke-width="3" stroke="#ED6A5A" d="M81.495,13.923c-11.368-5.261-26.234-0.311-31.489,11.032C44.74,13.612,29.879,8.657,18.511,13.923  C6.402,19.539,0.613,33.883,10.175,50.804c6.792,12.04,18.826,21.111,39.831,37.379c20.993-16.268,33.033-25.344,39.819-37.379  C99.387,33.883,93.598,19.539,81.495,13.923z"/>
              </svg>
            </div>
          </div>
        </div>
        <div id="buttonInfo" class="row">
        </div>
      </div>
    </div>
  </div>
</div>
