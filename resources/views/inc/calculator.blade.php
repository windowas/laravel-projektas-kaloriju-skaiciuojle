<div id="thecalculator" class="modal">
  <div class='col-md-4 col-md-offset-4 fullwBg'>
    <table>
      <tr>
        <td align=center>
          <br>
          <h2><b>Kalorijų skaičiuoklė</b></h2>
          <br>
          <p>
            Įveskite savo duomenis
          </p>
          <br>
          <form name="frm" action="" class='frms noborders'>
            <table>
              <tr>
                <td class="skctext">Amžius:</td>
                <td>
                  <input type="number" name="age" id="age" align="left" min="1" max="120">
                </td>
                <td  class="skctext">m.</td>
              </tr>
              <tr>
                <td class="skctext">Lytis:</td>
                <td>
                  <div class="row height60">
                    <div class="toggle-radio">
                      <input type="radio" name="gen" class="gen" id="male" checked>
                      <input type="radio" name="gen" class="gen" id="female">
                      <div class="switch">
                        <label for="male">Vyras</label>
                        <label for="female">Moteris</label>
                        <span></span>
                      </div>
                    </div>
                  </div>
                </td>
              </tr>
              <tr>
                <td class="skctext">Ūgis:</td>
                <td>
                  <input type="number" name="cen" id="cen" size="4">
                </td>
                <td>cm</td>
              </tr>
              <tr>
                <td class="skctext">Svoris:</td>
                <td>
                  <input type="number" name="weight" id="weight" maxlength="3" size="3">
                </td>
                <td>Kg</td>
              </tr>
              <tr>
                <td class="skctext">Kaip dažnai sportuojate:</td>
                <td>
                  <select name="loa" id="loa">
                    <option value="1">Nesportuoju</option>
                    <option value="2">1-2 kartai per savaitę</option>
                    <option value="3">3 kartai per savaitę</option>
                    <option value="4">3-4 kartai per savaitę</option>
                    <option value="5">Kasdien</option>
                  </select>
                </td>
              </tr>
              <tr>
                <td colspan='2' align="center">
                    <input id='calcBtn' class="btnEdit" type="button" value="Suskaičiuoti"><span id="dumdiv" align="center">
                </td>
            </tr>
        </table>
      </form>
      <br>
      <table align="center" border="0" class="frms noborders">
          <b class="skctext">Norint pasiekti norimus rezultatus</b>
          <tr>
              <td class="skctext" align="right">Jums reikia suvartoti:</td>
              <td>
                  <input type="text" id="rc" readonly></td>
                  <td>
                      <select name="caltype" id="caltype" onChange="convert()">
                          <option value="g">gramai</option>
                          <option value="kg">kilogramai</option>
                      </select>
                  </td>
              </tr>
          </table>
          <div class="skctext">Kasdien turėtumėte suvartoti</div>
          <table class="frms noborders">
              <tr>
                  <td class="skctext">Riebalų:
                  </td>
                  <td >
                      <input type="text" id="rf" readonly>
                  </td>
                  <td><font class="skctext"><label id="l1"></label> per dieną</font>
                  </td>
              </tr>
              <tr>
                  <td class="skctext">Baltymų:</td>
                  <td>
                      <input type="text" id="rp" readonly>
                  </td>
                  <td><font class="skctext"><label id="l2"></label> per dieną</font>
                  </td>
              </tr>
              <tr>
                  <td class="skctext">Angliavandenių:</td>
                  <td>
                      <input type="text" id="rh" readonly>
                  </td>
                  <td><font class="skctext">
                      <label id="l3"></label> per dieną</font>
                  </td>
              </tr>
          </table>
        </tr>
      </td>
    </table>
  </div>
</div>
