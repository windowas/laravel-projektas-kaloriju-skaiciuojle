@extends('layouts.app')

@section('content')
<div class="row wBg">
  <div class="page_header">
      <h1>{{$user->name}}</h1>
  </div>
  <table class="table">
      <tr>
          <td>Amžius:</td>
          <td>{{$user->age}} m.</td>
      </tr>
      <tr>
          <td>Lytis:</td>
          <td>
              @if($user->gen == 0)
                  Vyras
              @else
                  Moteris
               @endif
          </td>
      </tr>
      <tr>
          <td>Ūgis:</td>
          <td>{{$user->cen}} cm</td>
      </tr>
      <tr>
          <td>Svoris:</td>
          <td>{{$user->weight}} kg</td>
      </tr>
      <tr>
          <td>Aktyvumas:</td>
          <td>
              @if ($user->loa == 0)
                  Nesportuoju
              @elseif ($user->loa == 1)
                  1-2 kartai per savaitę
              @elseif ($user->loa == 2)
                  3 kartai per savaitę
              @elseif ($user->loa == 3)
                  3-4 kartai per savaitę
              @else
                  Kasdien
              @endif
          </td>
      </tr>
  </table>
  <a href="/user/{{$user->id}}/edit"><button class="btn btnEdit" type="button" name="button">Pakeisti duomenis</button></a>
  <div id="cneed">
    <h2><strong>Jums reikia suvartoti
          {{$user->cneed}}
          kilokalorijų
      </strong><h2>
  </div>
</div>

@include('inc.progress')

@include('inc.modal')

<script src="{{ asset('js/progressbar.js') }}"></script>
<div class="hidden">
{{-- ===================== Setting circles value ============= --}}
    @if($user->cneed == null)
        {{$cvalue = 0}}
    @elseif($user->consumed/$user->cneed <= 1)
        {{$cvalue = $user->consumed/$user->cneed}}
    @elseif($user->consumed/$user->cneed > 1)
        {{$cvalue = 1}}
    @endif

    @if($user->pneed == null)
        {{$pvalue = 0}}
    @elseif($user->pconsumed/$user->pneed <= 1)
        {{$pvalue = $user->pconsumed/$user->pneed}}
    @elseif($user->pconsumed/$user->pneed > 1)
        {{$pvalue = 1}}
    @endif

    @if($user->fneed == null)
        {{$fvalue = 0}}
    @elseif($user->fconsumed/$user->fneed <= 1)
        {{$fvalue = $user->fconsumed/$user->fneed}}
    @elseif($user->fconsumed/$user->fneed > 1)
        {{$fvalue = 1}}
    @endif

    @if($user->crneed == null)
        {{$crvalue = 0}}
    @elseif($user->crconsumed/$user->crneed <= 1)
        {{$crvalue = $user->crconsumed/$user->crneed}}
    @elseif($user->crconsumed/$user->crneed > 1)
        {{$crvalue = 1}}
    @endif

</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
// ================== Main circles ===============
  var mainCircle = new ProgressBar.Path('#heart-path', {
    easing: 'easeInOut',
    duration: 1400
  });
mainCircle.set(0);
mainCircle.animate({{$cvalue}});

var ProteinCircle = new ProgressBar.Path('#heart-paths-protein', {
  easing: 'easeInOut',
  duration: 1400
});
ProteinCircle.set(0);
ProteinCircle.animate({{$pvalue}});

var FatCircle = new ProgressBar.Path('#heart-paths-fat', {
  easing: 'easeInOut',
  duration: 1400
});
FatCircle.set(0);
FatCircle.animate({{$fvalue}});

var CarbonCircle = new ProgressBar.Path('#heart-paths-carbon', {
  easing: 'easeInOut',
  duration: 1400
});
CarbonCircle.set(0);
CarbonCircle.animate({{$crvalue}});
// ===================== Modal circles =========================
  var modalMainCircle = new ProgressBar.Path('#heart-paths', {
    easing: 'easeInOut',
    duration: 1400
  });
  modalMainCircle.set(0);
  modalMainCircle.animate({{$cvalue}});

  var modalProteinCircle = new ProgressBar.Path('#heart-paths-second-protein', {
    easing: 'easeInOut',
    duration: 1400
  });
  modalProteinCircle.set(0);
  modalProteinCircle.animate({{$pvalue}});

  var modalFatCircle = new ProgressBar.Path('#heart-paths-second-fat', {
    easing: 'easeInOut',
    duration: 1400
  });
  modalFatCircle.set(0);
  modalFatCircle.animate({{$fvalue}});

  var modalCarbonCircle = new ProgressBar.Path('#heart-paths-second-carbon', {
    easing: 'easeInOut',
    duration: 1400
  });
  modalCarbonCircle.set(0);
  modalCarbonCircle.animate({{$crvalue}});
</script>
<script>
// ============================== Variables =============================
var circval = 0;
var modbtn = $('.clickable');
var reset = $('#reset');

// ======================== Modal button push event =====================
  modbtn.on("click", progress);
  reset.on("click", resetProgress);

// ================= The HardCore Stuff =================
    function progress(){
      // ============================= Geting input values ===========
        var labels = $("#labelForSecondCircle");
        var inputs = $(this).children('input');
         arr = [];
         for(var i=0, len=inputs.length; i<len; i++){
           if(inputs[i].type === "hidden"){
             arr.push(inputs[i].value);
           }
         }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url: "/ajaxpost",
            data: {'pconsumed': arr[0], 'fconsumed': arr[1], 'crconsumed': arr[2],'consumed': arr[3]},
            success: function(data){
              // ===================== Main Circles ================
                if (data.usercon/data.userneed <= 1){
                  circval = data.usercon/data.userneed;
                  mainCircle.animate(circval);
                  modalMainCircle.animate(circval);}
                else if (data.usercon/data.userneed > 1){
                  circval = 1;
                  mainCircle.animate(circval);
                  modalMainCircle.animate(circval);
                }
                //====================== Protein Circles ==================
                if (data.userpcon/data.userpneed <= 1){
                  circval = data.userpcon/data.userpneed;
                  modalProteinCircle.animate(circval);
                  ProteinCircle.animate(circval);}
                else if (data.userpcon/data.userpneed > 1){
                  circval = 1;
                  modalProteinCircle.animate(circval);
                  ProteinCircle.animate(circval);
                }
                //====================== Fat Circles ==================
                if (data.userfcon/data.userfneed <= 1){
                  circval = data.userfcon/data.userfneed;
                  modalFatCircle.animate(circval);
                  FatCircle.animate(circval);}
                else if (data.userfcon/data.userfneed > 1){
                  circval = 1;
                  modalFatCircle.animate(circval);
                  FatCircle.animate(circval);
                }
                //====================== Carbon Circles ==================
                if (data.usercrcon/data.usercrneed <= 1){
                  circval = data.usercrcon/data.usercrneed;
                  modalCarbonCircle.animate(circval);
                  CarbonCircle.animate(circval);}
                else if (data.usercrcon/data.usercrneed > 1){
                  circval = 1;
                  modalCarbonCircle.animate(circval);
                  CarbonCircle.animate(circval);
                }
                // =============== Counters ==============
                // =================== Main =================
                $('.counterMain').each(function() {
                  var $this = $(this),
                      countTo = Math.floor(data.usercon);
                  $({ countNum: $this.text()}).animate({
                    countNum: countTo
                  },
                  {
                    duration: 1000,
                    easing:'linear',
                    step: function() {
                      $this.text(Math.floor(this.countNum));
                    },
                    complete: function() {
                      $this.text(this.countNum);
                    }
                  });
                });
                //  ================ Protein ==============
                $('.counterProtein').each(function() {
                  var $this = $(this),
                      countTo = Math.floor(data.userpcon);
                  $({ countNum: $this.text()}).animate({
                    countNum: countTo
                  },
                  {
                    duration: 1000,
                    easing:'linear',
                    step: function() {
                      $this.text(Math.floor(this.countNum));
                    },
                    complete: function() {
                      $this.text(this.countNum);
                    }
                  });
                });
                // =================== Fat =============
                $('.counterFat').each(function() {
                  var $this = $(this),
                      countTo = Math.floor(data.userfcon);
                  $({ countNum: $this.text()}).animate({
                    countNum: countTo
                  },
                  {
                    duration: 1000,
                    easing:'linear',
                    step: function() {
                      $this.text(Math.floor(this.countNum));
                    },
                    complete: function() {
                      $this.text(this.countNum);
                    }
                  });
                });
                // ============== Carbon ============
                $('.counterCarbon').each(function() {
                  var $this = $(this),
                      countTo = Math.floor(data.usercrcon);
                  $({ countNum: $this.text()}).animate({
                    countNum: countTo
                  },
                  {
                    duration: 1000,
                    easing:'linear',
                    step: function() {
                      $this.text(Math.floor(this.countNum));
                    },
                    complete: function() {
                      $this.text(this.countNum);
                    }
                  });
                });

                // ============== End success func ===============
            }
        });
    }
  function resetProgress(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url: "/ajaxpost",
            data: {'reset': 1},
            success: function(data){
              // =============== Reset circles =========
                circval = data.usercon/data.userneed;
                mainCircle.animate(circval);
                modalMainCircle.animate(circval);
                circval = data.userpcon/data.userpneed;
                modalProteinCircle.animate(circval);
                ProteinCircle.animate(circval);
                circval = data.userfcon/data.userfneed;
                modalFatCircle.animate(circval);
                FatCircle.animate(circval);
                circval = data.usercrcon/data.usercrneed;
                modalCarbonCircle.animate(circval);
                CarbonCircle.animate(circval);
// ===================Counters reset =================
// =============== Main =================
                $('.counterMain').each(function() {
                  var $this = $(this),
                      countTo = 0;
                  $({ countNum: $this.text()}).animate({
                    countNum: countTo
                  },
                  {
                    duration: 1000,
                    easing:'linear',
                    step: function() {
                      $this.text(Math.floor(this.countNum));
                    },
                    complete: function() {
                      $this.text(this.countNum);
                    }
                  });
                });
                // ============== Protein ==============
                $('.counterProtein').each(function() {
                  var $this = $(this),
                      countTo = 0;
                  $({ countNum: $this.text()}).animate({
                    countNum: countTo
                  },
                  {
                    duration: 1000,
                    easing:'linear',
                    step: function() {
                      $this.text(Math.floor(this.countNum));
                    },
                    complete: function() {
                      $this.text(this.countNum);
                    }
                  });
                });
                // =================== Fat =============
                $('.counterFat').each(function() {
                  var $this = $(this),
                      countTo = 0;
                  $({ countNum: $this.text()}).animate({
                    countNum: countTo
                  },
                  {
                    duration: 1000,
                    easing:'linear',
                    step: function() {
                      $this.text(Math.floor(this.countNum));
                    },
                    complete: function() {
                      $this.text(this.countNum);
                    }
                  });
                });
                // ============== Carbon ============
                $('.counterCarbon').each(function() {
                  var $this = $(this),
                      countTo = 0;
                  $({ countNum: $this.text()}).animate({
                    countNum: countTo
                  },
                  {
                    duration: 1000,
                    easing:'linear',
                    step: function() {
                      $this.text(Math.floor(this.countNum));
                    },
                    complete: function() {
                      $this.text(this.countNum);
                    }
                  });
                });
            }
        });
    }
</script>
@endsection
