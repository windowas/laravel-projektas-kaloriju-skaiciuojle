@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                {!! Form::open(['action' => ['HomeController@update', $user->id], 'method' => 'POST']) !!}
                <div class="form-group">
                    {{Form::label('age', 'Amžius')}}
                    {{Form::text('age', $user->age, ['class' => 'form-control'])}}
                </div>
                <div class="form-group">
                    {{Form::label('gen', 'Lytis: ')}}
                    {{-- {{Form::radio('gen', 0, $user->gen == 0)}}Vyras
                    {{Form::radio('gen', 1,$user->gen == 1)}}Moteris --}}
                    <div class="row height60">

                    <div class="toggle-radio">
                      @if($user->gen == 0 || $user->gen == null )
                      <input type="radio" name="gen" value=0 class="gen" id="male" checked>
                      <input type="radio" name="gen" value=1 class="gen" id="female">
                      @else
                      <input type="radio" name="gen" value=0 class="gen" id="male">
                      <input type="radio" name="gen" value=1 class="gen" id="female" checked>
                      @endif
                      <div class="switch">
                        <label for="male">Vyras</label>
                        <label for="female">Moteris</label>
                        <span></span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                    {{Form::label('cen', 'Ūgis')}}
                    {{Form::text('cen', $user->cen, ['class' => 'form-control'])}}
                </div>
                <div class="form-group">
                    {{Form::label('weight', 'Svoris')}}
                    {{Form::text('weight', $user->weight, ['class' => 'form-control'])}}
                </div>
                <div class="form-group">
                    {{Form::label('loa', 'Aktyvumas')}}
                    {{Form::select('loa', array('Nesportuoju', '1-2 kartai per savaitę', '3 kartai per savaitę', '3-4 kartai per savaitę', 'Kasdien'), $user->loa)}}
                </div>
                <div class="form-group">
                    {{Form::hidden('_method', 'PUT')}}
                    {{Form::submit('Atnaujinti', ['class' => 'btn btnEdit'])}}
                    {!! Form::close() !!}
                    <a class="btn btnCancel pull-right" href={{url()->previous()}}>Atšaukti</a>
                </div>
            </div>
        </div>
    </div>
@endsection
