@extends('layouts.app')

@section('content')
    @if($user->age == null || $user->gen == null || $user->cen == null || $user->weight == null || $user->loa == null)
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                {!! Form::open(['action' => ['HomeController@update', $user->id], 'method' => 'POST']) !!}
                <div class="form-group">
                    {{Form::label('age', 'Amžius')}}
                    {{Form::text('age', $user->age, ['class' => 'form-control'])}}
                </div>
                <div class="form-group">
                    {{Form::label('gen', 'Lytis: ')}}
                    {{Form::radio('gen', 'Vyras', $user->gen == 0)}}Vyras
                    {{Form::radio('gen', 'Moteris',$user->gen == 1)}}Moteris
                </div>
                <div class="form-group">
                    {{Form::label('cen', 'Ūgis')}}
                    {{Form::text('cen', $user->cen, ['class' => 'form-control'])}}
                </div>
                <div class="form-group">
                    {{Form::label('weight', 'Svoris')}}
                    {{Form::text('weight', $user->weight, ['class' => 'form-control'])}}
                </div>
                <div class="form-group">
                    {{Form::label('loa', 'Aktyvumas')}}
                    {{Form::select('loa', array('Nesportuoju', '1-2 kartai per savaitę', '3 kartai per savaitę', '3-4 kartai per savaitę', 'Kasdien'), $user->loa)}}
                </div>
                <div class="form-group">
                    {{Form::submit('Atnaujinti', ['class' => 'btn btn-primary'])}}
                    {!! Form::close() !!}
                    <a class="btn btn-default pull-right" href="/home">Atšaukti</a>
                </div>
            </div>
        </div>
    </div>
@endif
@endsection
