@extends('layouts.app')
@section('content')
  <div class="content">
    <div class="row">
      <div class="col-md-4">
        <img class="btn" id="callCalc" src="{{asset('images/calculator.png')}}" alt="Calculator">
        @include("inc.calculator")
      </div>
      <div class="col-md-8 wBg">
        <h2>Sveiki atvykę į Kalorijų skaičiuoklę!</h2>
        <p>
          Šis tinklalapis yra sukurtas palengvinti Jūsų kelią savo tikslo link. Užsiregistruokite, įveskite savo duomenis (amžių, lytį, ūgį, aktyvumo lygį) ir norimą pasiekti svorį – puslapyje matysite, kiek kilo kalorijų turėtumėte gauti iš maisto, norėdami tiek sverti. Paspauskite ant širdelės „Kalorijos“ – atsidarys naujas langas, kuriame galėsite pažymėti, kiek ir kokio maisto šiandien jau suvalgėte. Kai visa širdelė nusidažys raudonai, žinosite, kad šiandienos uždavinys pasiektas! Trys mažosios širdelės „Baltymai“, „Riebalai“ ir „Angliavandeniai“ skaičiuoja kiek kiekvienos iš šių medžiagų buvo Jūsų suvalgytame maiste, tad galėsite sekti, kokių produktų reikėtų vartoti daugiau, norint išlaikyti subalansuotą mitybą bei kiek dar trūksta iki Jums reikiamos dienos normos. Jeigu matote, kad viena iš širdelių pilnėja lėčiau, užeikite pele ant produktų pavadinimų ir po širdelėmis pamatysite, kiek įvairūs produktai tūri šių medžiagų. Taip galėsite pakoreguoti savo valgiaraštį. Rytoj kalorijas skaičiuokite ir vėl! Nepamirškite „Iš naujo nustatyti progresą.
        </p>
      </div>
    </div>
  </div>
@endsection
